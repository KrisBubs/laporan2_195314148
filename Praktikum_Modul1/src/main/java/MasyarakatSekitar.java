public class MasyarakatSekitar extends Penduduk {
    private int nomor;
    public MasyarakatSekitar() {
        
    }
    

    public MasyarakatSekitar(int nomor,String dataNim, String dataNama,String dataTempatTanggalLahir) {
        this.nomor = nomor;
    }

    public void setNomor(int nomor) {
        this.nomor = nomor;
    }

    public int getNomor() {
        return nomor;
    }
    
    @Override
    public double hitungIuran() {
        return getNomor() * 100;
        
    }

}