
public class Main {

    public static void main(String[] args) {

        UKM ukm = new UKM();
        Penduduk[]anggota;
        anggota = new Penduduk [10];
        int n = 0;
        
        System.out.println("UKM Sanata Dharma ");
        System.out.println("------------------------------------------------");
        
        Mahasiswa ketua = new Mahasiswa ();
        ketua.setNama("Kris");
        ketua.setNim(195314148);
        ketua.setTtl("Cilegon, 17 Februari 2001");

        Mahasiswa sekre = new Mahasiswa ();
        sekre.setNama("Winnie");
        sekre.setNim(195314087);
        sekre.setTtl("Pekalongan, 6 Juni 2001");
        
        ukm.setKetua(ketua);
        ukm.setSekretaris(sekre);
        ukm.setNamaUnit("Panitia");
        System.out.println("" +ukm.getNamaUnit());
        System.out.println("-------------------------------------");
        System.out.println("Ketua");
        System.out.println("Ketua\t\t : " +ukm.getKetua().getNama());
        System.out.println("NIM\t\t : " +ketua.getNim());
        System.out.println("Tempat Tanggal Lahir\t : "+ketua.getTtl());
        System.out.println("");
        System.out.println("Sekertaris");
        System.out.println("Sekretaris\t : " +ukm.getSekretaris().getNama());
        System.out.println("NIM\t\t : " +sekre.getNim());
        System.out.println("Tempat Tanggal Lahir\t : "+sekre.getTtl());
        System.out.println("-------------------------------------");
        System.out.println("");
        
        anggota[n] = new MasyarakatSekitar();
        ((MasyarakatSekitar)anggota[n]).setNama("Abdul");
        ((MasyarakatSekitar)anggota[n]).setNomor(113);
        ((MasyarakatSekitar)anggota[n]).setTtl("Jogja, 9 Maret 1994");
        n++;
        
        anggota[n] = new MasyarakatSekitar();
        ((MasyarakatSekitar)anggota[n]).setNama("Wati");
        ((MasyarakatSekitar)anggota[n]).setNomor(142);
        ((MasyarakatSekitar)anggota[n]).setTtl("Wonosobo, 13 Mei 1990");
        n++;
        
        anggota[n] = new Mahasiswa();
        ((Mahasiswa)anggota[n]).setNama("Felix");
        ((Mahasiswa)anggota[n]).setNim(195324135);
        ((Mahasiswa)anggota[n]).setTtl("Solo, 28 November 2000");
        n++;
        
        anggota[n] = new Mahasiswa();
        ((Mahasiswa)anggota[n]).setNama("Jon");
        ((Mahasiswa)anggota[n]).setNim(195314137);
        ((Mahasiswa)anggota[n]).setTtl("Banjarmasin, 14 April 2001");
        n++;
        
        anggota[n] = new Mahasiswa();
        ((Mahasiswa)anggota[n]).setNama("Nirma");
        ((Mahasiswa)anggota[n]).setNim(195314007);
        ((Mahasiswa)anggota[n]).setTtl("Pontianak, 2 Juli 2001");
        n++;

    
        System.out.println("Anggota");
        for (int i = 0; i < n; i++) {
            if(anggota[i]instanceof Mahasiswa){
                System.out.println("Mahasiswa");
                System.out.println("Nama\t\t: "+anggota[i].getNama());
                System.out.println("NIM\t\t: "+((Mahasiswa)anggota[i]).getNim());
                System.out.println("Tanggal Lahir\t: "+anggota[i].getTtl());
                System.out.println("Iuran\t\t: Rp"+((Mahasiswa)anggota[i]).hitungIuran());  
                System.out.println("");
            }
            else {
                System.out.println("Masyarakat Sekitar");
                 System.out.println("Nama\t\t: "+anggota[i].getNama());
                System.out.println("NoKtp\t\t: "+((MasyarakatSekitar)anggota[i]).getNomor());
                System.out.println("Tanggal Lahir\t: "+anggota[i].getTtl());
                System.out.println("Iuran\t\t: Rp"+((MasyarakatSekitar)anggota[i]).hitungIuran());
                System.out.println("");
            
            }
        }
    }
}
