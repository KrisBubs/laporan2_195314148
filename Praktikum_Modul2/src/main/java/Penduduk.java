   public abstract class Penduduk {

    protected String nama, ttl;
    
    public Penduduk() {      
    }

    public Penduduk(String Nama, String ttl) {
        this.nama = nama;
        this.ttl = ttl;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setTtl(String ttl) {
        this.ttl = ttl;
    }

    public String getNama() {
        return nama;
    }

    public String getTtl() {
        return ttl;
    }
    
    public abstract double hitungIuran();
}
 

